{-# LANGUAGE OverloadedStrings #-}
module Home where

import Post
import Text.StringTemplate
import System.Directory
import Site
import Constant
import qualified Data.Text as T
import qualified Data.Text.IO as T

homeTemplate = do
    templates <- directoryGroup "templates" :: IO (STGroup T.Text)
    return $ maybe (newSTMP "generic.st not found")
                   id
                   (getStringTemplate "generic" templates)

renderGenericHome :: Site -> StringTemplate T.Text -> [T.Text] -> T.Text
renderGenericHome s t cs = render
                       $ setAttribute "site" s
                       $ setAttribute "content" (T.unlines cs) t

renderHome :: IO T.Text
renderHome = do
    cs <- getAllExtracts
    t <- homeTemplate
    s <- readSite
    return $ renderGenericHome s t $ reverse cs

writeHome :: T.Text -> IO ()
writeHome s = do
    mkdir $ sitePath ++ "home/"
    writeFile (sitePath ++ "home/index.html") (T.unpack s)
    writeFile (sitePath ++ "index.html") (T.unpack s)

generateHome = renderHome >>= writeHome >> putStrLn "Generated home"
