module Server where

import Happstack.Server ( Browsing (EnableBrowsing), nullConf
                        , serveDirectory, simpleHTTP
                        )
import Constant

startServer :: IO ()
startServer = simpleHTTP nullConf
            $ serveDirectory EnableBrowsing ["index.html"] sitePath
