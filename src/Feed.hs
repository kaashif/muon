{-# LANGUAGE OverloadedStrings #-}
module Feed where

import Post
import qualified Site as S
import Constant
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Time.Format
import Data.Time.Clock
import System.Locale
import Data.Maybe (fromJust)

format :: T.Text -> T.Text
format t = T.pack $ formatTime defaultTimeLocale "%a, %d %b %Y %X -0000" (time :: UTCTime)
    where time = fromJust $ parseTime defaultTimeLocale "%Y-%m-%d" (T.unpack t)

itemize :: S.Site -> Post -> T.Text
itemize s p = T.concat [
    "<item>",
    "<title>",
    (title p),
    "</title>",
    "<link>",
    (S.url s),
    (link p),
    "</link>",
    "<description>",
    (comment p),
    "</description>",
    "<pubDate>",
    (format $ date p),
    "</pubDate>",
    "<guid>",
    (S.url s),
    (link p),
    "</guid>",
    "</item>"]

makeRSS :: S.Site -> [Post] -> T.Text
makeRSS s ps = T.unlines [
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>",
    "<rss version=\"2.0\"><channel>",
    "<title>",
    (S.title s),
    "</title>",
    "<link>",
    (S.url s),
    "</link>",
    "<description>",
    (S.tagline s),
    "</description>",
    (T.concat $ map (itemize s) ps),
    "</channel></rss>"]

writeFeed :: IO ()
writeFeed = do
    site <- S.readSite
    posts <- getAllPosts
    T.writeFile (sitePath ++ "rss.xml") (makeRSS site posts)
