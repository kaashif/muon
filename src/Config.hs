{-# LANGUAGE OverloadedStrings #-}
module Config (getSetting, rsyncCmd) where

import System.Directory
import Data.ConfigFile
import Data.Either.Utils
import qualified Data.Text as T

blogCP :: IO ConfigParser
blogCP = fmap forceEither $ readfile emptyCP "config.ini"

getSetting :: String -> String -> IO String
getSetting sect c = do
    cp <- blogCP
    return $ forceEither $ get cp sect c

rsyncCmd :: IO String
rsyncCmd = do
    user <- getSetting "remote" "user"
    server <- getSetting "remote" "server"
    dir <- getSetting "remote" "dir"
    return $ "rsync -av --delete site/ " ++ user ++ "@" ++ server ++ ":" ++ dir
