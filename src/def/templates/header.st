<!DOCTYPE html>
<html>
<head>
<title>$site.title$</title>
<link rel="stylesheet" type="text/css" href="$site.style$">
<script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<script 
  src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=sunburst">
</script>
</head>
<body>
<div class="wrapper">
<main>
<header>
        <h1>$site.title$</h1>
        <p>$site.tagline$</p>
        <ul>
            <li> <a href="/home">Home</a></li>
            <li>//</li>
            <li> <a href="/about">About</a></li>
            <li>//</li>
            <li> <a href="/contact">Contact</a></li>
            <li>//</li>
            <li> <a href="/archive">Archive</a></li>
        </ul>
</header>
<hr id="header" />
<div class="content">
