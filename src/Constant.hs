{-# LANGUAGE OverloadedStrings #-}
module Constant where

import System.Directory
import qualified Data.Text as T

sitePath :: FilePath
sitePath = "site/"
siteDirs = ["site", "style", "posts", "templates", "pages"]
mkdir = (createDirectoryIfMissing True)
