{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
module Site where

import qualified Data.Text as T
import Data.Typeable
import Data.Data
import Data.ConfigFile
import Data.Either.Utils
import Config

data Site = Site { title :: T.Text
                 , style :: T.Text
                 , author :: T.Text
                 , tagline :: T.Text
                 , url :: T.Text
                 } deriving (Data, Typeable, Show)

defaultSite = Site { title = "Default Blog"
                   , style = "/style.css"
                   , author = "Your Name"
                   , tagline = "Description of this blog"
                   , url = "http://example.com"
                   }

readSite :: IO Site
readSite = do
    t <- getSetting "site" "title"
    s <- getSetting "site" "style"
    a <- getSetting "site" "author"
    g <- getSetting "site" "tagline"
    u <- getSetting "site" "url"
    return $ Site { title = T.pack t
                  , style = T.pack s
                  , author = T.pack a
                  , tagline = T.pack g
                  , url = T.pack u
                  }
