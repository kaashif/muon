Muon
====
Muon is a static site generator, meaning that it takes files written in
convenient markup and converts it to HTML and CSS ready to deploy to a
web server.

Note
----
I don't actually use Muon any more. Please see
[here](http://hg.kaashif.co.uk/hgweb/tau/file) for what I actually use now.
