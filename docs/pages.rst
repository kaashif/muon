Adding Pages
============

In addition to a homepage, archive, and posts, you might want some extra
pages on your site, like "example.com/recipes" or "example.com/laptop".
You can add such pages to your site simply by creating a file with the
right name in the "pages/" directory of your blog.

For example, if your blog is at "myblog.com" and you want a page at
"myblog.com/mypage", edit the file "pages/mypage" and fill it with
Markdown. Bear in mind, this content will go in between the "header" and
"footer" templates, so you don't need to include \<body\> or \<html\> tags.
Here's what you might put in "pages/mypage":

.. code::

    ## My Additional Page
    This is a page I made!

And that's that! Next time you ``muon generate upload``, the page will be
accessible at "myblog.com/mypage", or (if you run ``muon generate serve``)
at "127.0.0.1:8000/mypage".
