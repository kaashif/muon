Muon Documentation
==================

Muon is a static site generator, similar in function to Jekyll, Hakyll, Hyde,
and many others. Muon is different to the majority of other static site
generators because it is written in Haskell, which is a language that compiles
to native code, thus is faster, as it doesn't require a bulky interpreter to run
every time you want to itch your nose. Also, there aren't hundreds of
dependencies you don't want and will never use! Because of this, configuring and
using Muon is very simple.

Contents
--------

.. toctree::
   :maxdepth: 2

   install
   quick
   config
   pages
   posts
