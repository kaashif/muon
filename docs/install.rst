Installing
==========

Installing from GitHub
----------------------
.. warning:: The version on GitHub may not be performant, lack features, or
             simply be broken and trash your site's source. Install at your
             own peril.


First, install the package by getting the source repo.

.. code::

    $ git clone https://github.com/kaashif-hymabaccus/muon.git

You can install it using cabal, which you should have installed.

.. code::

    $ cd muon
    $ cabal install

After that, assuming you have configured cabal and/or your PATH
correctly, muon should be usable.

Installing from Hackage
-----------------------
Installing from Hackage using Cabal is as simple as:

.. code::

    $ cabal install muon

Bear in mind this may not be the most up-to-date version, but it will be
a stable version.
