Writing Posts
=============

To add a new post, create a new file in the "posts/" directory with the
suffix ".post".

When writing posts, make sure you put the title on the first line, the
date on the second, and a short description (for the archive) on the
third line. The rest should be valid Markdown. See the "posts/"
directory after site initialisation for some examples.

In the archive, posts are ordered lexicographically, *not* by date. This
means "aaa.post" will always come above "bbb.post", regardless of the
date contained in the file itself. This allows you to decide the order
of your posts yourself, if you want to separate tutorials and rants, for
example.
