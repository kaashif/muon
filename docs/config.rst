Configuration
=============

After ``muon init`` is run, a file called "config.ini" is created. This is
where you configure the blog. By default, it should look like this:

.. code::

    [site]
    title=Default site
    author=Your Name
    tagline=Something descriptive
    style=/style.css

    [remote]
    user=root
    server=webserver
    dir=/var/www/htdocs/

The [site] section needs little explanation - you can preview the site
yourself and see where those strings go. The "style" option denotes the
location of a custom CSS file.

The [remote] section is to configure the command run by Muon to upload
the site. The command is ``rsync -a --delete site/ user@server:dir``, with
the key words replaced with your SSH credentials on a server. For this
to work as you expect, the directory has to end in a slash.
