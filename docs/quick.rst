Quick Start
===========

Initialise a default site:

.. code::

    $ mkdir new-blog
    $ cd new-blog
    $ muon init

Write a post:

.. code::

    $ vi posts/new.post

Regenerate the site, creating a tree of HTML documents in the ./site directory.

.. code::

    $ muon generate

Preview the site locally, on "http://127.0.0.1:8000":

.. code::

    $ muon serve

Upload site to server (the credentials for which are specified in the
blog's "config.ini" file):

.. code::

    $ muon upload

Multiple commands in sequence:

.. code::

    $ muon init generate upload
    $ muon generate serve
